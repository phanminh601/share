﻿using System;
using System.Collections.Generic;

namespace ManageEmployeeDataAccessLayer.DataObject;

public partial class Activityy
{
    public int IdActivity { get; set; }

    public string NameActivity { get; set; } = null!;

    public string? Description { get; set; }

    public DateTime? ParticipationDate { get; set; }

    public string? Status { get; set; }

    public virtual ICollection<EmployeeGroup> EmployeeGroups { get; set; } = new List<EmployeeGroup>();
}
