﻿using System;
using System.Collections.Generic;

namespace ManageEmployeeDataAccessLayer.DataObject;

public partial class EmployeeRole
{
    public int? RoleId { get; set; }

    public int? IdRole { get; set; }

    public int EmployeeRolecolId { get; set; }

    public virtual Role? IdRoleNavigation { get; set; }

    public virtual Employee? Role { get; set; }
}
