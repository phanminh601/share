﻿using System;
using System.Collections.Generic;

namespace ManageEmployeeDataAccessLayer.DataObject;

public partial class Role
{
    public int IdRole { get; set; }

    public string NameRole { get; set; } = null!;

    public string? Description { get; set; }

    public virtual ICollection<EmployeeRole> EmployeeRoles { get; set; } = new List<EmployeeRole>();
}
