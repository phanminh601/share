﻿using System;
using System.Collections.Generic;

namespace ManageEmployeeDataAccessLayer.DataObject;

public partial class Groupp
{
    public int IdGroup { get; set; }

    public string NameGroup { get; set; } = null!;

    public string Leader { get; set; } = null!;

    public string? Description { get; set; }

    public virtual ICollection<EmployeeGroup> EmployeeGroups { get; set; } = new List<EmployeeGroup>();
}
