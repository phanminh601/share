﻿using System;
using System.Collections.Generic;

namespace ManageEmployeeDataAccessLayer.DataObject;

public partial class EmployeeGroup
{
    public int? EmployeeId { get; set; }

    public int? IdGroup { get; set; }

    public int? IdActivity { get; set; }

    public int EmployeeGroupId { get; set; }

    public virtual Employee? Employee { get; set; }

    public virtual Activityy? IdActivityNavigation { get; set; }

    public virtual Groupp? IdGroupNavigation { get; set; }
}
