﻿using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeDataAccessLayer.Data
{
    public partial class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<ManageEmployeeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Activityy> Activityys { get; set; }

        public virtual DbSet<Employee> Employees { get; set; }

        public virtual DbSet<EmployeeGroup> EmployeeGroups { get; set; }

        public virtual DbSet<EmployeeRole> EmployeeRoles { get; set; }

        public virtual DbSet<Groupp> Groupps { get; set; }

        public virtual DbSet<Role> Roles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
            => optionsBuilder.UseMySQL("server=localhost;port=3306;uid=root;pwd=phan.thanh.minh;database=ManageEmployee");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activityy>(entity =>
            {
                entity.HasKey(e => e.IdActivity).HasName("PRIMARY");

                entity.ToTable("activityy");

                entity.Property(e => e.IdActivity).HasColumnName("ID_Activity");
                entity.Property(e => e.Description).HasColumnType("text");
                entity.Property(e => e.NameActivity).HasMaxLength(100);
                entity.Property(e => e.ParticipationDate)
                    .HasColumnType("date")
                    .HasColumnName("Participation_date");
                entity.Property(e => e.Status).HasMaxLength(50);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PRIMARY");

                entity.ToTable("employee");

                entity.HasIndex(e => e.ManagerId, "Manager_id");

                entity.Property(e => e.Id).HasColumnName("ID");
                entity.Property(e => e.Address).HasMaxLength(255);
                entity.Property(e => e.Avatar).HasMaxLength(255);
                entity.Property(e => e.DateOfBirth)
                    .HasColumnType("date")
                    .HasColumnName("Date_of_birth");
                entity.Property(e => e.Email).HasMaxLength(100);
                entity.Property(e => e.Gender).HasMaxLength(10);
                entity.Property(e => e.ManagerId).HasColumnName("Manager_id");
                entity.Property(e => e.Name).HasMaxLength(100);
                entity.Property(e => e.Password).HasMaxLength(100);
                entity.Property(e => e.Phonenumber).HasMaxLength(20);
                entity.Property(e => e.Salary).HasPrecision(10);
                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.Manager).WithMany(p => p.InverseManager)
                    .HasForeignKey(d => d.ManagerId)
                    .HasConstraintName("employee_ibfk_1");
            });

            modelBuilder.Entity<EmployeeGroup>(entity =>
            {
                entity.HasKey(e => e.EmployeeGroupId).HasName("PRIMARY");

                entity.ToTable("employee_group");

                entity.HasIndex(e => e.IdActivity, "ID_Activity");

                entity.HasIndex(e => e.EmployeeId, "employee_group_ibfk_1");

                entity.HasIndex(e => e.IdGroup, "employee_group_ibfk_2");

                entity.Property(e => e.EmployeeGroupId).HasColumnName("employee_group_id");
                entity.Property(e => e.EmployeeId).HasColumnName("Employee_ID");
                entity.Property(e => e.IdActivity).HasColumnName("ID_Activity");
                entity.Property(e => e.IdGroup).HasColumnName("ID_Group");

                entity.HasOne(d => d.Employee).WithMany(p => p.EmployeeGroups)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("employee_group_ibfk_1");

                entity.HasOne(d => d.IdActivityNavigation).WithMany(p => p.EmployeeGroups)
                    .HasForeignKey(d => d.IdActivity)
                    .HasConstraintName("employee_group_ibfk_3");

                entity.HasOne(d => d.IdGroupNavigation).WithMany(p => p.EmployeeGroups)
                    .HasForeignKey(d => d.IdGroup)
                    .HasConstraintName("employee_group_ibfk_2");
            });

            modelBuilder.Entity<EmployeeRole>(entity =>
            {
                entity.HasKey(e => e.EmployeeRolecolId).HasName("PRIMARY");

                entity.ToTable("employee_role");

                entity.HasIndex(e => e.RoleId, "employee_role_ibfk_1");

                entity.HasIndex(e => e.IdRole, "employee_role_ibfk_2");

                entity.Property(e => e.EmployeeRolecolId).HasColumnName("employee_rolecol_id");
                entity.Property(e => e.IdRole).HasColumnName("ID_Role");
                entity.Property(e => e.RoleId).HasColumnName("Role_id");

                entity.HasOne(d => d.IdRoleNavigation).WithMany(p => p.EmployeeRoles)
                    .HasForeignKey(d => d.IdRole)
                    .HasConstraintName("employee_role_ibfk_2");

                entity.HasOne(d => d.Role).WithMany(p => p.EmployeeRoles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("employee_role_ibfk_1");
            });

            modelBuilder.Entity<Groupp>(entity =>
            {
                entity.HasKey(e => e.IdGroup).HasName("PRIMARY");

                entity.ToTable("groupp");

                entity.Property(e => e.IdGroup).HasColumnName("ID_Group");
                entity.Property(e => e.Description).HasColumnType("text");
                entity.Property(e => e.Leader).HasMaxLength(100);
                entity.Property(e => e.NameGroup).HasMaxLength(100);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.IdRole).HasName("PRIMARY");

                entity.ToTable("role");

                entity.Property(e => e.IdRole).HasColumnName("ID_Role");
                entity.Property(e => e.Description).HasColumnType("text");
                entity.Property(e => e.NameRole).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
