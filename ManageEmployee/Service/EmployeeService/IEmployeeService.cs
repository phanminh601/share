﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;

namespace ManageEmployee.Service.EmployeeService
{
    public interface IEmployeeService
    {
        Task<ServiceResponse<EmployeeIdTheir>> getEmployeeIdthier(int employeeId);
        Task<ServiceResponse<EmployeeDto>> updateEmployeeId(int employeeId, UEmployeeDto uEmployeeDto);

       // Task<ServiceResponse<EmployeeDto>> getEmployeeGroupId(int employeeId);
        Task<ServiceResponse<List<string>>> GetEmployeeGroupNameAsync(int employeeId);
        Task<ServiceResponse<UEmployeeDto>> getEmployeeIdOther(int employeeId);
        Task<ServiceResponse<EmployeeDto>> updatepass(int MEmployeeId, string newPass);
    }
}

