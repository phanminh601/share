﻿using AutoMapper;
using Google.Apis.Drive.v3.Data;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployeeDataAccessLayer.Data;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;
using System.Security.Claims;

namespace ManageEmployee.Service.EmployeeService
{
    public class EmployeeServicelmpl : IEmployeeService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public EmployeeServicelmpl(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        //public async Task<ServiceResponse<EmployeeDto>> getEmployeeGroupId(int employeeId)
        //{
        //    ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();
        //    var getEGId = _dataContext.Employees.FindAsync(employeeId).Result;
        //    if(getEGId == null)
        //    {
        //        serviceResponse.ErrorCode = 404;
        //        serviceResponse.Status = false;
        //        serviceResponse.Message = "Employee is not valid";
                
        //        return serviceResponse;
        //    }
        //    var nameGroup = await(from e in _dataContext.Employees
        //                          join eg in _dataContext.EmployeeGroups on e.Id equals eg.EmployeeId
        //                          join g in _dataContext.Groupps on eg.IdGroup equals g.IdGroup
        //                          where e.Id == getEGId.Id
        //                          select g.NameGroup).ToListAsync();
            
        //    if (nameGroup == null)
        //    {
        //        serviceResponse.ErrorCode = 404;
        //        serviceResponse.Status = false;
        //        serviceResponse.Message = "This employee does not belong to any group";
        //        return serviceResponse;
        //    }
        //    //serviceResponse.Data = nameGroup;
        //    //serviceResponse.ToString(nameGroup);
        //    serviceResponse.Status = true;
        //    serviceResponse.Message = "Name Group information retrieved successfully.";
        //    return serviceResponse;
        //}

        public async Task<ServiceResponse<EmployeeIdTheir>> getEmployeeIdthier(int employeeId)
        {

            ServiceResponse<EmployeeIdTheir> serviceResponse = new ServiceResponse<EmployeeIdTheir>();
            //var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var dbgetE = await _dataContext.Employees.ToListAsync();
            var getE = dbgetE.Select(e => _mapper.Map<EmployeeIdTheir>(e)).Where(c => c.Id == employeeId).FirstOrDefault();
            if (getE == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid";
                return serviceResponse;
            }

            serviceResponse.Data = getE;
            serviceResponse.Status = true;
            serviceResponse.Message = "Employee information retrieved successfully.";
            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeDto>> updateEmployeeId(int employeeId, UEmployeeDto uEmployeeDto)
        {
            ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();
            var updateEmployeeId = await _dataContext.Employees.FindAsync(employeeId);
            if (updateEmployeeId == null)
            {
                // Nếu không tìm thấy nhân viên, trả về lỗi
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid";
                return serviceResponse;
            }
            updateEmployeeId.Name = uEmployeeDto.Name;
            updateEmployeeId.Gender = uEmployeeDto.Gender;
            updateEmployeeId.DateOfBirth = uEmployeeDto.Date_of_birth;
            updateEmployeeId.Address = uEmployeeDto.Address;
            updateEmployeeId.Phonenumber = uEmployeeDto.Phonenumber;
            updateEmployeeId.Email = uEmployeeDto.Email;

            _dataContext.Employees.Update(updateEmployeeId);
            await _dataContext.SaveChangesAsync();

            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Employee updated successfully";
            //serviceResponse.Data = _mapper.Map<EmployeeDto>(updateEmployeeId);

            return serviceResponse;
        }
        public async Task<ServiceResponse<List<string>>> GetEmployeeGroupNameAsync(int employeeId)
        {
            ServiceResponse<List<string>> serviceResponse = new ServiceResponse<List<string>>();

            var getEGId = await _dataContext.Employees.FindAsync(employeeId);
            if (getEGId == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid";
                return serviceResponse;
            }

            var nameGroup = await (from e in _dataContext.Employees
                                   join eg in _dataContext.EmployeeGroups on e.Id equals eg.EmployeeId
                                   join g in _dataContext.Groupps on eg.IdGroup equals g.IdGroup
                                   where e.Id == getEGId.Id
                                   select g.NameGroup).ToListAsync();

            if (nameGroup == null || nameGroup.Count == 0)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "This employee does not belong to any group";
                return serviceResponse;
            }

            serviceResponse.Data = nameGroup;
            serviceResponse.Status = true;
            serviceResponse.Message = "Name Group information retrieved successfully.";
            return serviceResponse;
        }

        public async Task<ServiceResponse<UEmployeeDto>> getEmployeeIdOther(int employeeId)
        {
            ServiceResponse<UEmployeeDto> serviceResponse = new ServiceResponse<UEmployeeDto>();
            var dbgetEOther = await _dataContext.Employees.ToListAsync();
            var getEOther = dbgetEOther.Select(e => _mapper.Map<UEmployeeDto>(e)).Where(c => c.ID == employeeId).FirstOrDefault();
            if (getEOther == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid";
                return serviceResponse;
            }

            serviceResponse.Data = getEOther;
            serviceResponse.Status = true;
            serviceResponse.Message = "Employee information retrieved successfully.";
            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeDto>> updatepass(int MEmployeeId, string newPass)
        {
            var serviceResponse = new ServiceResponse<EmployeeDto>();

            var employee = await _dataContext.Employees.FindAsync(MEmployeeId);

            employee.Password = BCrypt.Net.BCrypt.HashPassword(newPass);

            _dataContext.Employees.Update(employee);
            await _dataContext.SaveChangesAsync();

            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Password resset successfully";
            return serviceResponse;
        }
    }
}
