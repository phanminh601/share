﻿//using ManageEmployee.DTOs.Request;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using System.Threading.Tasks;

namespace ManageEmployee.Service.ManageEmployee
{
    public interface IManageEmployee
    {
        Task<ServiceResponse<PageList<EmployeeIdTheir>>> GetAllEmployee(OwnerParameters ownerParameters);
        Task<ServiceResponse<EmployeeIdTheir>> GetAllEmployeeId(int MEmployeeId);
        Task<ServiceResponse<EmployeeDto>> createMEmployee(CreateEmployeeDto createEmployeeDto);
        Task<ServiceResponse<EmployeeDto>> updateMEmployee(UpdateEmployeeDto updateEmployeeDto, int MEmployeeId);
        Task<ServiceResponse<EmployeeDto>> deleteMEmployee(int MEmployeeId);
        Task<ServiceResponse<EmployeeDto>> addEmployeeGroup(AddEmployee addEmployee, int MEmployeeId);
        Task<ServiceResponse<EmployeeDto>> addEmployeeRole(AddEmployee addEmployee, int MEmployeeId);
        Task<ServiceResponse<EmployeeDto>> addEmployeeActivity(AddEmployeeActivity addEmployeeActivity, int MEmployeeId);
        //Task<ServiceResponse<List<string>>> viewEmployee(int MEmployeeId);
    }
}
