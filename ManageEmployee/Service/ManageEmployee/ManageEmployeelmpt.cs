﻿using AutoMapper;
using DocumentFormat.OpenXml.Office2010.Excel;
using Google.Apis.Drive.v3.Data;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Request;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Tls;
using ServiceStack;
using System.Linq;
using Tensorflow;

namespace ManageEmployee.Service.ManageEmployee
{
    public class ManageEmployeelmpt : IManageEmployee
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ManageEmployeelmpt(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<EmployeeDto>> addEmployeeActivity(AddEmployeeActivity addEmployeeActivity, int MEmployeeId)
        {
            ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();
            var employeeId = _dataContext.Employees.FirstOrDefaultAsync(e => e.Id == MEmployeeId).Result;

            if (employeeId == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid!!";
                return serviceResponse;
            }
            var eGN = _dataContext.Groupps.FirstOrDefaultAsync(g => g.IdGroup == addEmployeeActivity.IdGroup).Result;

            if (eGN == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Name Group is not valid!!";
                return serviceResponse;
            }
            // ktra name group 
            var eAN = _dataContext.Activityys.FirstOrDefaultAsync(g => g.IdActivity == addEmployeeActivity.IdACtivity).Result;

            if (eAN == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Activity Group is not valid!!";
                return serviceResponse;
            }
            // ktra
            var group = _dataContext.EmployeeGroups
           .FirstOrDefaultAsync(
            eg => eg.EmployeeId == MEmployeeId
            && eg.IdGroup == addEmployeeActivity.IdGroup
            && eg.IdActivity == null)
           .Result;

            // kiểm tra tồn tại cặp group 
            if (group != null)
            {
                var groupUpdate = _dataContext.EmployeeGroups.FirstOrDefaultAsync(eg => eg.IdGroup == addEmployeeActivity.IdGroup
                && eg.EmployeeId == MEmployeeId).Result;

                groupUpdate.IdActivity = addEmployeeActivity.IdACtivity;
                _dataContext.EmployeeGroups.Update(groupUpdate);
                await _dataContext.SaveChangesAsync();
                serviceResponse.ErrorCode = 200;
                serviceResponse.Status = true;
                serviceResponse.Message = "Activity updated successfully";
            }

            var group3 = _dataContext.EmployeeGroups
            .FirstOrDefaultAsync(
            eg => eg.EmployeeId == MEmployeeId
            && eg.IdGroup == addEmployeeActivity.IdGroup
            && eg.IdActivity == addEmployeeActivity.IdACtivity)
            .Result;
            // kiểm tra tồn tại cặp threee
            if (group3 != null)
            {
                serviceResponse.ErrorCode = 200;
                serviceResponse.Status = true;
                serviceResponse.Message = "Employee Group Activity already exist!";
                return serviceResponse;
            }
            var EmployeeA = new EmployeeGroup
            {
                EmployeeId = MEmployeeId,
                IdGroup = addEmployeeActivity.IdGroup,
                IdActivity = addEmployeeActivity.IdACtivity
            };
            var newEmployeeA = _mapper.Map<EmployeeGroup>(EmployeeA);
            if (group3 == null)
            {
                _dataContext.EmployeeGroups.Add(newEmployeeA);
                await _dataContext.SaveChangesAsync();
                serviceResponse.ErrorCode = 200;
                serviceResponse.Status = true;
                serviceResponse.Message = "Activity add successfully!!";
                return serviceResponse;
            }   
            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeDto>> addEmployeeGroup(AddEmployee addEmployee, int MEmployeeId)
        {
            ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();
            var employeeId = _dataContext.Employees.FirstOrDefaultAsync(e => e.Id == MEmployeeId).Result;

            if (employeeId == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid!!";
                return serviceResponse;
            }
            // ktra id
            var eGN = _dataContext.Groupps.FirstOrDefaultAsync(g => g.IdGroup == addEmployee.Id).Result;

            if (eGN == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Name Group is not valid!!";
                return serviceResponse;
            }
            // ktra name group 

            var group = _dataContext.EmployeeGroups
                  .FirstOrDefaultAsync(
                  eg => eg.EmployeeId == MEmployeeId
               && eg.IdGroup == addEmployee.Id)
                  .Result;

            //if (A != null && B != null) 
            if (group != null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee and group already exist!!";
                return serviceResponse;
            }
            var EmployeeG = new EmployeeGroup
            {
                EmployeeId = MEmployeeId,
                IdGroup = addEmployee.Id
            };
            var newEmployee = _mapper.Map<EmployeeGroup>(EmployeeG);
            if (group == null)
            {
                _dataContext.EmployeeGroups.Add(newEmployee);
                await _dataContext.SaveChangesAsync();
                serviceResponse.ErrorCode = 200;
                serviceResponse.Status = true;
                serviceResponse.Message = "Employee Group add successfully!!";
                return serviceResponse;
            }
            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeDto>> addEmployeeRole(AddEmployee addEmployee, int MEmployeeId)
        {
            ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();
            var employeeId = _dataContext.Employees.FirstOrDefaultAsync(e => e.Id == MEmployeeId).Result;

            if (employeeId == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid!!";
                return serviceResponse;
            }
            // ktra id
            var eGN = _dataContext.Roles.FirstOrDefaultAsync(g => g.IdRole == addEmployee.Id).Result;

            if (eGN == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Name Role is not valid!!";
                return serviceResponse;
            }
            // ktra name group 

            var group = _dataContext.EmployeeRoles
                  .FirstOrDefaultAsync(
                  eg => eg.RoleId == MEmployeeId
               && eg.IdRole == addEmployee.Id)
                  .Result;

            //if (A != null && B != null) 
            if (group != null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee and role already exist!!";
                return serviceResponse;
            }
            var EmployeeR = new EmployeeRole
            {
                RoleId = MEmployeeId,
                IdRole = addEmployee.Id
            };
            var newEmployee = _mapper.Map<EmployeeRole>(EmployeeR);
            if (group == null)
            {
                _dataContext.EmployeeRoles.Add(EmployeeR);
                await _dataContext.SaveChangesAsync();
                serviceResponse.ErrorCode = 200;
                serviceResponse.Status = true;
                serviceResponse.Message = "Employee Role add successfully!!";
                return serviceResponse;
            }
            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeDto>> createMEmployee(CreateEmployeeDto createEmployeeDto)
        {
            ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            var existingEmployee = _dataContext.Employees.FirstOrDefault(e => e.Name == createEmployeeDto.Name);
            if (existingEmployee != null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Duplicate employee name";
                return serviceResponse;
            }

            if (createEmployeeDto.Avatar != null)
            {
                var avatarImage = _dataContext.Employees.FirstOrDefault(e => e.Avatar == createEmployeeDto.Avatar);
                if (avatarImage != null)
                {
                    serviceResponse.ErrorCode = 404;
                    serviceResponse.Status = false;
                    serviceResponse.Message = "Avatar image is duplicated";
                    return serviceResponse;
                }
            }
            //Employee newEmployee = _mapper.Map<Employee>(createEmployeeDto);

            createEmployeeDto.Password = BCrypt.Net.BCrypt.HashPassword(createEmployeeDto.Password);

            Employee newEmployee = _mapper.Map<Employee>(createEmployeeDto);

            _dataContext.Employees.Add(newEmployee);
            await _dataContext.SaveChangesAsync();

            return serviceResponse;
        }
        public async Task<ServiceResponse<EmployeeDto>> deleteMEmployee(int MEmployeeId)
        {
            ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();

            // Tìm kiếm danh mục cần xóa
            var employeeToDelete = await _dataContext.Employees.FindAsync(MEmployeeId);

            // Kiểm tra xem danh mục có tồn tại hay không
            if (employeeToDelete == null)
            {
                // Nếu không tìm thấy danh mục, cập nhật thông tin trong đối tượng serviceResponse
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee not found";
                return serviceResponse;
            }

            // Xóa thông tin danh mục trong cơ sở dữ liệu
            _dataContext.Employees.Remove(employeeToDelete);
            await _dataContext.SaveChangesAsync();

            // Cập nhật thông tin trong đối tượng serviceResponse
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Employee deleted successfully";

            return serviceResponse;
        }


        public async Task<ServiceResponse<PageList<EmployeeIdTheir>>> GetAllEmployee(OwnerParameters ownerParameters)
        {

            ServiceResponse<PageList<EmployeeIdTheir>> serviceResponse = new ServiceResponse<PageList<EmployeeIdTheir>>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            // Lấy danh sách nhân viên từ cơ sở dữ liệu
            var dbEmployee = await _dataContext.Employees.ToListAsync();

            // Chuyển đổi danh sách nhân viên sang danh sách EmployeeLRDto
            var EmployeeIdTheir = dbEmployee.Select(u => _mapper.Map<EmployeeIdTheir>(u)).ToList();

            serviceResponse.Data = PageList<EmployeeIdTheir>.ToPageList(
            EmployeeIdTheir.AsEnumerable<EmployeeIdTheir>(),
            ownerParameters.pageIndex,
            ownerParameters.pageSize);

            // Trả về ServiceResponse
            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeIdTheir>> GetAllEmployeeId(int MEmployeeId)
        {
            ServiceResponse<EmployeeIdTheir> serviceResponse = new ServiceResponse<EmployeeIdTheir>();
            var dbgetEOther = await _dataContext.Employees.ToListAsync();
            var getEOther = dbgetEOther.Select(e => _mapper.Map<EmployeeIdTheir>(e)).Where(c => c.Id == MEmployeeId).FirstOrDefault();
            if (getEOther == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid";
                return serviceResponse;
            }

            serviceResponse.Data = getEOther;
            serviceResponse.Status = true;
            serviceResponse.Message = "Employee information retrieved successfully.";
            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeDto>> updateMEmployee(UpdateEmployeeDto updateEmployeeDto, int MEmployeeId)
        {
            ServiceResponse<EmployeeDto> serviceResponse = new ServiceResponse<EmployeeDto>();
            var employeeUpdate = _dataContext.Employees.FirstOrDefaultAsync(e => e.Id == MEmployeeId).Result;
            if (employeeUpdate == null)
            {
                // Nếu không tìm thấy nhân viên, trả về lỗi
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Employee is not valid";
                return serviceResponse;
            }
            // //var employee = await _dataContext.Employees.FindAsync(MEmployeeId);

            //_mapper.Map(updateEmployeeDto, employeeUpdate);

            // Đánh dấu đối tượng đã thay đổi để Entity Framework Core cập nhật

            // _dataContext.Entry(employee).State = EntityState.Modified;

            employeeUpdate.Name = updateEmployeeDto.Name;
            employeeUpdate.Gender = updateEmployeeDto.Gender;
            employeeUpdate.DateOfBirth = updateEmployeeDto.Date_of_birth;
            employeeUpdate.Address = updateEmployeeDto.Address;
            employeeUpdate.Phonenumber = updateEmployeeDto.Phonenumber;
            employeeUpdate.Email = updateEmployeeDto.Email;
            employeeUpdate.Salary = updateEmployeeDto.Salary;
            employeeUpdate.ManagerId = updateEmployeeDto.Manager_id;
            employeeUpdate.Avatar = updateEmployeeDto.Avatar;

            _dataContext.Employees.Update(employeeUpdate);
            await _dataContext.SaveChangesAsync();

            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Employee updated successfully";
            serviceResponse.Data = _mapper.Map<EmployeeDto>(employeeUpdate);

            return serviceResponse;
        }
        //public async Task<ServiceResponse<List<string>>> GetEmployeeGroupNameAsync(int employeeId)
        //    ServiceResponse<List<string>> serviceResponse = new ServiceResponse<List<string>>();
        //public async Task<ServiceResponse<List<string>>> viewEmployee(int MEmployeeId)
        //{
        //    ServiceResponse<List<string>> serviceResponse = new ServiceResponse<List<string>>();
        //    var views = await _dataContext.Employees.FindAsync(MEmployeeId);
        //    if (views == null)
        //    {
        //        serviceResponse.ErrorCode = 404;
        //        serviceResponse.Status = false;
        //        serviceResponse.Message = "Employee not found";
        //        return serviceResponse;
        //    }
        //    var nameG = await (from e in _dataContext.Employees
        //                       join eg in _dataContext.EmployeeGroups on e.Id equals eg.EmployeeId
        //                       join g in _dataContext.Groupps on eg.IdGroup equals g.IdGroup
        //                       where e.Id == MEmployeeId
        //                       select g.NameGroup).ToListAsync();

        //    var nameA = await (from e in _dataContext.Employees
        //                       join eg in _dataContext.EmployeeGroups on e.Id equals eg.EmployeeId
        //                       join a in _dataContext.Activityys on eg.IdActivity equals a.IdActivity
        //                       where e.Id == MEmployeeId
        //                       select a.NameActivity).ToListAsync();

        //    var nameR = await (from e in _dataContext.Employees
        //                       join er in _dataContext.EmployeeRoles on e.Id equals er.RoleId
        //                       join r in _dataContext.Roles on er.IdRole equals r.IdRole
        //                       where e.Id == MEmployeeId
        //                       select r.NameRole).ToListAsync();

        //    var combinedList = new List<string>();
        //    combinedList.AddRange(nameG);
        //    combinedList.AddRange(nameA);
        //    combinedList.AddRange(nameR);

        //    serviceResponse.Data = combinedList;
        //    serviceResponse.Status = true;
        //    serviceResponse.Message = "Employee position chart information retrieved successfully.";
        //    return serviceResponse;
        //}


    }
}
