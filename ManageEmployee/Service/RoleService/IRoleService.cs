﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;

namespace ManageEmployee.Service.RoleService
{
    public interface IRoleService
    {
        Task<ServiceResponse<PageList<RoleDto>>> GetAllRole(OwnerParameters ownerParameters);
        Task<ServiceResponse<RoleDto>> createRole(CreateRoleDto createRoleDto);
        Task<ServiceResponse<RoleDto>> updateRole(CreateRoleDto createRoleDto, int roleId);
        Task<ServiceResponse<RoleDto>> deleteRole(int roleId);

    }
}
