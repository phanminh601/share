﻿using AutoMapper;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.EntityFrameworkCore;

namespace ManageEmployee.Service.RoleService
{
    public class RoleServicelmpl : IRoleService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public RoleServicelmpl(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<RoleDto>> createRole(CreateRoleDto createRoleDto)
        {
            ServiceResponse<RoleDto> serviceResponse = new ServiceResponse<RoleDto>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            var exRole = _dataContext.Roles.FirstOrDefault(g => g.NameRole == createRoleDto.NameRole);
            if (exRole != null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Duplicate role name";
                return serviceResponse;
            }
            var newRole = _mapper.Map<Role>(createRoleDto);

            _dataContext.Roles.Add(newRole);
            await _dataContext.SaveChangesAsync();
            return serviceResponse;
        }

        public async Task<ServiceResponse<RoleDto>> deleteRole(int roleId)
        {
            ServiceResponse<RoleDto> serviceResponse = new ServiceResponse<RoleDto>();
            var roleToDelete = await _dataContext.Roles.FindAsync(roleId);

            if (roleToDelete == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Role not found";
                return serviceResponse;
            }
            _dataContext.Roles.Remove(roleToDelete);
            await _dataContext.SaveChangesAsync();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Role deleted successfully";
            return serviceResponse;
        }

        public async Task<ServiceResponse<PageList<RoleDto>>> GetAllRole(OwnerParameters ownerParameters)
        {
            ServiceResponse<PageList<RoleDto>> serviceResponse = new ServiceResponse<PageList<RoleDto>>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            // Lấy danh sách nhân viên từ cơ sở dữ liệu
            var dbRole = await _dataContext.Roles.ToListAsync();

            // Chuyển đổi danh sách nhân viên sang danh sách EmployeeLRDto
            var RoleDto = dbRole.Select(u => _mapper.Map<RoleDto>(u)).ToList();

            serviceResponse.Data = PageList<RoleDto>.ToPageList(
            RoleDto.AsEnumerable<RoleDto>(),
            ownerParameters.pageIndex,
            ownerParameters.pageSize);  

            // Trả về ServiceResponse
            return serviceResponse;
        }

        public async Task<ServiceResponse<RoleDto>> updateRole(CreateRoleDto createRoleDto, int roleId)
        {
            ServiceResponse<RoleDto> serviceResponse = new ServiceResponse<RoleDto>();
            var roleUpdate = _dataContext.Roles.FirstOrDefaultAsync(e => e.IdRole == roleId).Result;
            if (roleUpdate == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Role is not valid";
                return serviceResponse;
            }
            var role = await _dataContext.Roles.FindAsync(roleId);

            _mapper.Map(createRoleDto, role);

            _dataContext.Entry(role).State = EntityState.Modified;

            await _dataContext.SaveChangesAsync();

            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Role updated successfully";
            serviceResponse.Data = _mapper.Map<RoleDto>(role);

            return serviceResponse;
        }
    }
}
