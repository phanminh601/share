﻿using AutoMapper;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ManageEmployee.Service.ActivityService
{
    public class ActivityServicelmpl : IActivityService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ActivityServicelmpl(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<ActivityDto>> createActivity(CreateActivityDto createActivityDto)
        {
            ServiceResponse<ActivityDto> serviceResponse = new ServiceResponse<ActivityDto>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            var exActivity = _dataContext.Activityys.FirstOrDefault(g => g.NameActivity == createActivityDto.NameActivity);
            if (exActivity != null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Duplicate activity name";
                return serviceResponse;
            }
            var newActivity = _mapper.Map<Activityy>(createActivityDto);

            _dataContext.Activityys.Add(newActivity);
            await _dataContext.SaveChangesAsync();
            return serviceResponse;
        }
        public async Task<ServiceResponse<ActivityDto>> updateActivity(CreateActivityDto createActivityDto, int activityId)
        {
            ServiceResponse<ActivityDto> serviceResponse = new ServiceResponse<ActivityDto>();
            var activityUpdate = _dataContext.Activityys.FirstOrDefaultAsync(e => e.IdActivity == activityId).Result;
            if (activityUpdate == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Activity is not valid";
                return serviceResponse;
            }
            var activityy = await _dataContext.Activityys.FindAsync(activityId);

            _mapper.Map(createActivityDto, activityy);

            _dataContext.Entry(activityy).State = EntityState.Modified;

            await _dataContext.SaveChangesAsync();

            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Activity updated successfully";
            serviceResponse.Data = _mapper.Map<ActivityDto>(activityy);

            return serviceResponse;
        }
        public async Task<ServiceResponse<ActivityDto>> deleteActivity(int activityId)
        {
            ServiceResponse<ActivityDto> serviceResponse = new ServiceResponse<ActivityDto>();
            var activityToDelete = await _dataContext.Activityys.FindAsync(activityId);

            if (activityToDelete == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Activity not found";
                return serviceResponse;
            }
            _dataContext.Activityys.Remove(activityToDelete);
            await _dataContext.SaveChangesAsync();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Activity deleted successfully";
            return serviceResponse;
        }

        public async Task<ServiceResponse<PageList<ActivityDto>>> GetAllActivity(OwnerParameters ownerParameters)
        {
            ServiceResponse<PageList<ActivityDto>> serviceResponse = new ServiceResponse<PageList<ActivityDto>>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            // Lấy danh sách nhân viên từ cơ sở dữ liệu
            var dbActivity = await _dataContext.Activityys.ToListAsync();

            // Chuyển đổi danh sách nhân viên sang danh sách EmployeeLRDto
            var ActivityDto = dbActivity.Select(u => _mapper.Map<ActivityDto>(u)).ToList();

            serviceResponse.Data = PageList<ActivityDto>.ToPageList(
            ActivityDto.AsEnumerable<ActivityDto>(),
            ownerParameters.pageIndex,
            ownerParameters.pageSize);

            // Trả về ServiceResponse
            return serviceResponse;
        }

        
    }
}
