﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using System.Threading.Tasks;

namespace ManageEmployee.Service.ActivityService
{
    public interface IActivityService
    {
        Task<ServiceResponse<PageList<ActivityDto>>> GetAllActivity(OwnerParameters ownerParameters);
        Task<ServiceResponse<ActivityDto>> createActivity(CreateActivityDto createActivityDto);
        Task<ServiceResponse<ActivityDto>> updateActivity(CreateActivityDto createActivityDto, int activityId);
        Task<ServiceResponse<ActivityDto>> deleteActivity(int activityId);

    }
}
