﻿using AutoMapper;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.EntityFrameworkCore;

namespace ManageEmployee.Service.GroupService
{
    public class GroupServicelmpl : IGroupService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public GroupServicelmpl(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<GroupDto>> createGroup(CreateGroupDto createGroupDto)
        {
            ServiceResponse<GroupDto> serviceResponse =  new ServiceResponse<GroupDto>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            //var exGroup = _dataContext.Groupps.Where(g => g.NameGroup == createGroupDto.NameGroup).FirstAsync();
            var exGroup = _dataContext.Groupps.FirstOrDefault(g => g.NameGroup == createGroupDto.NameGroup);
            if (exGroup != null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Duplicate group name";
                return serviceResponse;
            }
            var newGroup = _mapper.Map<Groupp>(createGroupDto);

            _dataContext.Groupps.Add(newGroup);
            await _dataContext.SaveChangesAsync();
            return serviceResponse;
        }
        public async Task<ServiceResponse<GroupDto>> updateGroup(UpdateGroupDto updateGroup, int groupId)
        {
            ServiceResponse<GroupDto> serviceResponse = new ServiceResponse<GroupDto>();
            var groupUpdate = _dataContext.Groupps.FirstOrDefaultAsync(e => e.IdGroup == groupId).Result;
            if (groupUpdate == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Group is not valid";
                return serviceResponse;
            }
            var group = await _dataContext.Groupps.FindAsync(groupId);

            _mapper.Map(updateGroup, group);

            _dataContext.Entry(group).State = EntityState.Modified;

            await _dataContext.SaveChangesAsync();

            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Group updated successfully";
            serviceResponse.Data = _mapper.Map<GroupDto>(group);

            return serviceResponse;

        }
        public async Task<ServiceResponse<GroupDto>> deleteGroup(int groupId)
        {
            ServiceResponse<GroupDto> serviceResponse = new ServiceResponse<GroupDto>();
            var groupToDelete = await _dataContext.Groupps.FindAsync(groupId);

            if (groupToDelete == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "Group not found";
                return serviceResponse;
            }
            _dataContext.Groupps.Remove(groupToDelete);
            await _dataContext.SaveChangesAsync();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Group deleted successfully";
            return serviceResponse;
        }

        public async Task<ServiceResponse<PageList<GroupDto>>> GetAllGroup(OwnerParameters ownerParameters)
        {
            ServiceResponse<PageList<GroupDto>> serviceResponse = new ServiceResponse<PageList<GroupDto>>();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = true;
            serviceResponse.Message = "Successfully";

            // Lấy danh sách nhân viên từ cơ sở dữ liệu
            var dbGroup = await _dataContext.Groupps.ToListAsync();

            // Chuyển đổi danh sách nhân viên sang danh sách EmployeeLRDto
            var GroupDto = dbGroup.Select(u => _mapper.Map<GroupDto>(u)).ToList();

            serviceResponse.Data = PageList<GroupDto>.ToPageList(
            GroupDto.AsEnumerable<GroupDto>(),
            ownerParameters.pageIndex,
            ownerParameters.pageSize);

            // Trả về ServiceResponse
            return serviceResponse;
        }

       
    }
}
