﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;

namespace ManageEmployee.Service.GroupService
{
    public interface IGroupService
    {
        Task<ServiceResponse<PageList<GroupDto>>> GetAllGroup(OwnerParameters ownerParameters);
        Task<ServiceResponse<GroupDto>> createGroup(CreateGroupDto createGroupDto);
        Task<ServiceResponse<GroupDto>> updateGroup(UpdateGroupDto updateGroup, int groupId);
        Task<ServiceResponse<GroupDto>> deleteGroup(int groupId);
    }
}
