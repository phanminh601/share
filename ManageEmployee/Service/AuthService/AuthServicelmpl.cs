﻿using AutoMapper;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Google.Apis.Drive.v3.Data;
using Humanizer;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.HTTP.SSO.JWT;
using ManageEmployee.Models;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol.Plugins;
using Org.BouncyCastle.Tls;
using System.Security.Claims;
using System.Security.Principal;
using Tensorflow;

namespace ManageEmployee.Service.AuthService
{
    public class AuthServicelmpl : IAuthService
    {
        private readonly DataContext _dataContext;
        private readonly IConfiguration _configuration;
        private readonly IJwtTokenProvider _jwtTokenProvider;
        private readonly IMapper _mapper;

        public AuthServicelmpl(DataContext dataContext, IConfiguration configuration, IJwtTokenProvider jwtTokenProvider, IMapper mapper)
        {
            _dataContext = dataContext;
            _configuration = configuration;
            _jwtTokenProvider = jwtTokenProvider;
            _mapper = mapper;
        }

        //public async Task<ServiceResponse<EmployeeDto>> CreateAccount(LoginFrom loginFrom, int MEmployeeId)
        //{
        //    var serviceResponse = new ServiceResponse<EmployeeDto>();
        //    if (_dataContext.Employees.FirstOrDefault(
        //        e => e.Username == loginFrom.username) != null)
        //    {
        //        serviceResponse.Status = false;
        //        serviceResponse.ErrorCode = 400;
        //        serviceResponse.Message = "Tài khoản đã tồn tại !";
        //        return serviceResponse;
        //    }
        //    // tạo tài khoản
        //    loginFrom.password = BCrypt.Net.BCrypt.HashPassword(loginFrom.password);
        //    Employee employee = _mapper.Map<Employee>(loginFrom);

        //    var employeeAccount = await _dataContext.Employees.FindAsync(MEmployeeId);
        //    _mapper.Map(loginFrom, employeeAccount);
        //    //_dataContext.Entry(employee).State = EntityState.Modified;
        //    loginFrom.username = employeeAccount.Username;
        //   // loginFrom.password = employee.Password;

        //    _dataContext.Employees.Update(employeeAccount);
        //    await _dataContext.SaveChangesAsync();

        //    serviceResponse.ErrorCode = 200;
        //    serviceResponse.Status = false;
        //    serviceResponse.Message = "Account updated successfully";
        //    //serviceResponse.Data = _mapper.Map<EmployeeDto>(loginFrom);
        //    return serviceResponse;
        //}

        public async Task<IPrincipal> CreatePrincipal(Employee username)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,username.Id + "")
            };
            var identity = new ClaimsIdentity(claims);
            return new ClaimsPrincipal(identity);
        }

        public async Task<LoginResponse<List<BirthDayDto>>> Login(LoginFrom loginFrom)
        {
            LoginResponse<List<BirthDayDto>> loginResponse = new LoginResponse<List<BirthDayDto>>();
            var user = await _dataContext.Employees
                .FirstOrDefaultAsync(u => u.Username.Equals(loginFrom.username));
            if (user == null)
            {
                loginResponse.ErrorCode = 400;
                loginResponse.Status = false;
                loginResponse.Message = "Tài khoản không chính xác!";
                return loginResponse;
            }
            if (!VerifyPassword(loginFrom.password, user.Password))
            {
                loginResponse.ErrorCode = 400;
                loginResponse.Status = false;
                loginResponse.Message = "Mật khẩu không chính xác!";
                return loginResponse;
            }
            IPrincipal userlogin = await CreatePrincipal(user);
            var token = _jwtTokenProvider.CreateToken(userlogin);

            var userRole = (from e in _dataContext.Employees
                            join er in _dataContext.EmployeeRoles on e.Id equals er.RoleId
                            join r in _dataContext.Roles on er.IdRole equals r.IdRole
                            where e.Id == user.Id
                            select r.NameRole).FirstOrDefault();
            if (userRole == "Admin")
            {
                DateTime today = DateTime.Now;
                // Lấy thông tin các nhân viên có sinh nhật vào ngày hôm nay
                var employees = _dataContext.Employees.Where(e => e.DateOfBirth.HasValue
                && e.DateOfBirth.Value.Month == today.Month && e.DateOfBirth.Value.Day == today.Day).ToList();

                if (employees.Count == 0)
                {
                    loginResponse.ErrorCode = 404;
                    loginResponse.Status = false;
                    loginResponse.Message = "No one has a birthday today!!";
                    //return loginResponse;
                }
                else
                {
                    List<BirthDayDto> birthDayDtos = new List<BirthDayDto>();

                    foreach (var employee in employees)
                    {
                        BirthDayDto birthDayDto = new BirthDayDto
                        {
                            Id = employee.Id,
                            Name = employee.Name,
                            DateOfBirh = employee.DateOfBirth,
                        };

                        birthDayDtos.Add(birthDayDto);
                    }
                    loginResponse.Data = birthDayDtos;
                    loginResponse.Status = true;
                    loginResponse.Message = "Birthday information retrieved successfully!!!";
                }

            }
            else
            {
                DateTime today = DateTime.Now;
                var employeed = _dataContext.Employees.
                    Where(e => e.DateOfBirth.HasValue
                    && e.DateOfBirth.Value.Month == today.Month
                    && e.DateOfBirth.Value.Day == today.Day
                    && e.Username == loginFrom.username)
                    .ToList();
                if (employeed.Count != 0)
                {
                   // loginResponse.ErrorCode = 404;
                    loginResponse.Status = true;
                    loginResponse.Message = "Happy birth day to you!!";
                }
            }
            loginResponse.RoleName = userRole;
            loginResponse.accessToken = token;
            loginResponse.Id = user.Id;
            loginResponse.Messageed = "Login successfully!!!";
            return loginResponse;
        }
        //public async Task<LoginResponse> Login(LoginFrom loginFrom)
        //{
        //    var loginResponse = new LoginResponse();
        //    var user = await _dataContext.Employees
        //        .FirstOrDefaultAsync(u => u.Username.Equals(loginFrom.username));
        //    if (user == null)
        //    {
        //        loginResponse.StatusCode = 400;
        //        loginResponse.Status = false;
        //        loginResponse.Message = "Tài khoản không chính xác!";
        //        return loginResponse;
        //    }
        //    var userRole = (from e in _dataContext.Employees
        //                    join er in _dataContext.EmployeeRoles on e.Id equals er.RoleId
        //                    join r in _dataContext.Roles on er.IdRole equals r.IdRole
        //                    where e.Id == user.Id
        //                    select r.NameRole).FirstOrDefault();

        //    //if (!userRoles.Contains("Admin"))
        //    //{
        //    //    loginResponse.ErrorCode = 400;
        //    //    loginResponse.Status = false;
        //    //    loginResponse.Message = "Bạn không có quyền truy cập!";
        //    //    return loginResponse;
        //    //}

        //    if (!VerifyPassword(loginFrom.password, user.Password))
        //    {
        //        loginResponse.StatusCode = 400;
        //        loginResponse.Status = false;
        //        loginResponse.Message = "Mật khẩu không chính xác!";
        //        return loginResponse;
        //    }
        //    IPrincipal userlogin = await CreatePrincipal(user);
        // var token = _jwtTokenProvider.CreateToken(userlogin);
        //loginResponse.RoleName = userRole;
        ////var token = _jwtTokenProvider.CreateToken();
        //loginResponse.accessToken = token;
        //loginResponse.UserId = user.Id;
        //    return loginResponse;
        //}

        public async Task<ServiceResponse<EmployeeDto>> ResetAccount(string username, ResetPass resetPass)
        {
            var serviceResponse = new ServiceResponse<EmployeeDto>();

            var employee = _dataContext.Employees.Where(e => e.Username == username).FirstOrDefault();
            if (employee == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Message = "Employee not found";
                return serviceResponse;
            }
            var employer = _dataContext.Employees.FirstOrDefault(e => e.Username == username);
            if (!VerifyPassword(resetPass.currentPassword, employer.Password))
            {
                serviceResponse.ErrorCode = 400;
                serviceResponse.Message = "Current password is incorrect";
                return serviceResponse;
            }

            employee.Password = BCrypt.Net.BCrypt.HashPassword(resetPass.newPassword);
            _dataContext.Employees.Update(employee);

            await _dataContext.SaveChangesAsync();
            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Password resset successfully";

            return serviceResponse;
        }

        public async Task<ServiceResponse<EmployeeDto>> ResetAccountAdmin(int MEmployeeId, string newPass)
        {
            var serviceResponse = new ServiceResponse<EmployeeDto>();

            var employee = await _dataContext.Employees.FindAsync(MEmployeeId);
            if (employee == null)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Message = "Employee not found";
                return serviceResponse;
            }

            employee.Password = BCrypt.Net.BCrypt.HashPassword(newPass);
            // Employee employeer = _mapper.Map<Employee>(newPass);

            _dataContext.Employees.Update(employee);
            await _dataContext.SaveChangesAsync();

            serviceResponse.ErrorCode = 200;
            serviceResponse.Status = false;
            serviceResponse.Message = "Password resset successfully";
            // serviceResponse.Data = _mapper.Map<EmployeeDto>(loginFrom);

            return serviceResponse;
        }

        private bool VerifyPassword(string password, string storePassword)
        {
            return BCrypt.Net.BCrypt.Verify(password, storePassword);
        }
    }
}