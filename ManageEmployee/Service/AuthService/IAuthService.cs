﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;

namespace ManageEmployee.Service.AuthService
{
    public interface IAuthService
    {
        //Task<LoginResponse> Login(LoginFrom loginFrom);
        //Task<ServiceResponse<List<LoginResponse>>> Login(LoginFrom loginFrom);
        Task<LoginResponse<List<BirthDayDto>>> Login(LoginFrom loginFrom);
        //Task<ServiceResponse<EmployeeDto>> CreateAccount (LoginFrom loginFrom, int MEmployeeId);
        Task<ServiceResponse<EmployeeDto>> ResetAccount(string username, ResetPass resetPass);
        Task<ServiceResponse<EmployeeDto>> ResetAccountAdmin(int MEmployeeId, string newPass);


    }
}
