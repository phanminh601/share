﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;

namespace ManageEmployee.Service.BirthDayService
{
    public interface IBirthDayService
    {
        //Task<ServiceResponse<BirthDayDto>> getBrithDay();
        Task<ServiceResponse<List<BirthDayDto>>> getBrithDay();
        Task<ServiceResponse<List<BirthDayMonthDto>>> getBrithDayMonth();
    }
}
