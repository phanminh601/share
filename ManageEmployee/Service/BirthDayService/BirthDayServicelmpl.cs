﻿using AutoMapper;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;

namespace ManageEmployee.Service.BirthDayService
{
    public class BirthDayServicelmpl : IBirthDayService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public BirthDayServicelmpl(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }
        
        public async Task<ServiceResponse<List<BirthDayDto>>> getBrithDay()
        {
            ServiceResponse<List<BirthDayDto>> serviceResponse = new ServiceResponse<List<BirthDayDto>>();
            //ServiceResponse<BirthDayDto> serviceResponse = new ServiceResponse<BirthDayDto>();
            // Lấy ngày tháng năm hiện tại
            DateTime today = DateTime.Now;

            // Lấy thông tin các nhân viên có sinh nhật vào ngày hôm nay
            var employees = _dataContext.Employees.Where(e => e.DateOfBirth.HasValue 
            && e.DateOfBirth.Value.Month == today.Month && e.DateOfBirth.Value.Day == today.Day).ToList();

            if (employees.Count == 0)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "No one has a birthday today!!";
                return serviceResponse;
            }

            List<BirthDayDto> birthDayDtos = new List<BirthDayDto>();

            foreach (var employee in employees)
            {
                BirthDayDto birthDayDto = new BirthDayDto
                {
                    Id = employee.Id,
                    Name = employee.Name,
                    DateOfBirh = employee.DateOfBirth,
                };

                birthDayDtos.Add(birthDayDto);
            }
            serviceResponse.Data = birthDayDtos;
            serviceResponse.Status = true;
            serviceResponse.Message = "Birthday information retrieved successfully.";

            return serviceResponse;
        }
        public async Task<ServiceResponse<List<BirthDayMonthDto>>> getBrithDayMonth()
        {
            ServiceResponse<List<BirthDayMonthDto>> serviceResponse = new ServiceResponse<List<BirthDayMonthDto>>();
            //ServiceResponse<BirthDayDto> serviceResponse = new ServiceResponse<BirthDayDto>();
            // Lấy ngày tháng năm hiện tại
            DateTime today = DateTime.Now;

            // Lấy thông tin các nhân viên có sinh nhật vào tháng này
            var employeese = _dataContext.Employees.Where(e => e.DateOfBirth.HasValue && e.DateOfBirth.Value.Month == today.Month).ToList();

            if (employeese.Count == 0)
            {
                serviceResponse.ErrorCode = 404;
                serviceResponse.Status = false;
                serviceResponse.Message = "No one has a birthday month!!";
                return serviceResponse;
            }

            List<BirthDayMonthDto> birthDayMonthDtos = new List<BirthDayMonthDto>();

            foreach (var employee in employeese)
            {
                BirthDayMonthDto birthDayMonthDto = new BirthDayMonthDto
                {
                    Id = employee.Id,
                    Name = employee.Name,
                    DateOfBirh = employee.DateOfBirth,
                    CountBirth = employeese.Count
                };

                birthDayMonthDtos.Add(birthDayMonthDto);
            }

            serviceResponse.Data = birthDayMonthDtos;
            serviceResponse.Status = true;
            serviceResponse.Message = "Birthday information retrieved successfully.";

            return serviceResponse;
        }
    }
}
