﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployee.Service.GroupService;
using ManageEmployee.Service.ManageEmployee;
using ManageEmployeeDataAccessLayer.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Security.Claims;

namespace ManageEmployee.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    [Produces("application/json")]
    public class GroupManagerController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IGroupService _groupService;

        public GroupManagerController(DataContext dataContext, IGroupService groupService)
        {
            _dataContext = dataContext;
            _groupService = groupService;
        }
            //[Authorize]
        [HttpGet("GetAllGroup")]
        public async Task<ActionResult<ServiceResponse<PageList<GroupDto>>>> GetAllGroup([FromQuery] OwnerParameters ownerParameters)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _groupService.GetAllGroup(ownerParameters);
            var metadata = new
            {
                response.Data.TotalCount,
                response.Data.PageSize,
                response.Data.CurrentPage,
                response.Data.TotalPages,
                response.Data.HasNext,
                response.Data.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            return Ok(response);
        }
        //[Authorize]
        [HttpPost("CreateGroup")]
        public async Task<ActionResult<ServiceResponse<GroupDto>>> createGroup(CreateGroupDto createGroupDto)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _groupService.createGroup(createGroupDto);
            return Ok(response);
        }      
        //[Authorize]
        [HttpPut("UpdateGroup")]
        public async Task<ActionResult<ServiceResponse<GroupDto>>> updateGroup(UpdateGroupDto updateGroup, int groupId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _groupService.updateGroup(updateGroup, groupId);
            return Ok(response);
        }
        //[Authorize]
        [HttpDelete("DeleteGroup")]
        public async Task<ActionResult<ServiceResponse<GroupDto>>> deleteGroup(int groupId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _groupService.deleteGroup(groupId);
            return Ok(response);
        }
    }
}
