﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Service.AuthService;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Security.Principal;

namespace ManageEmployee.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IConfiguration _configuration;
        private readonly IAuthService _authService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthController(DataContext dataContext, IConfiguration configuration, IAuthService authService, IHttpContextAccessor httpContextAccessor)
        {
            _dataContext = dataContext;
            _configuration = configuration;
            _authService = authService;
            _httpContextAccessor = httpContextAccessor;
        }
        [HttpPost("Login")]
        // [AllowAnonymous]
        public async Task<ActionResult<LoginResponse<List<BirthDayDto>>>> Login([FromQuery] LoginFrom loginFrom)
        // public async Task<ActionResult<ServiceResponse<LoginResponse>>> Login([FromQuery] LoginFrom loginFrom)
        //public async Task<ActionResult<LoginResponse>> Login(
        //[FromBody] LoginForm loginForm)
        {
            var response = await _authService.Login(loginFrom);
            return Ok(response);
        }
        //[HttpPut("CreateAccount")]
        //public async Task<ActionResult<ServiceResponse<EmployeeDto>>> CreateAccount(LoginFrom loginFrom, int MEmployeeId)
        //{
        //    var response = await _authService.CreateAccount(loginFrom, MEmployeeId);
        //    return Ok(response);
        //}
        [Authorize]
        [HttpPut("ResetAccountAdmin")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> ResetAccountAdmin(int MEmployeeId, string newPass)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _authService.ResetAccountAdmin(MEmployeeId, newPass);
            return Ok(response);
        }
        //[Authorize]
        [HttpPut("ResetAccount")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> ResetAccount([FromQuery] string username, [FromQuery] ResetPass resetPass)
        {
            var response = await _authService.ResetAccount(username, resetPass);
            return Ok(response);
        }

    }
}
