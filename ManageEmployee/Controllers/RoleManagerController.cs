﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployee.Service.GroupService;
using ManageEmployee.Service.RoleService;
using ManageEmployeeDataAccessLayer.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Security.Claims;

namespace ManageEmployee.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    [Produces("application/json")]
    public class RoleManagerController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IRoleService _roleService;

        public RoleManagerController(DataContext dataContext, IRoleService roleService)
        {
            _dataContext = dataContext;
            _roleService = roleService;
        }
        //public async Task<ServiceResponse<PageList<RoleDto>>> GetAllRole(OwnerParameters ownerParameters
        [HttpGet("GetAllRole")]
        public async Task<ActionResult<ServiceResponse<PageList<RoleDto>>>> GetAllRole([FromQuery] OwnerParameters ownerParameters)
        {
            //var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            //string userRole = CheckPermission.Check(userId);
            //if (userRole != "Admin")
            //{
            //    return new StatusCodeResult(403);
            //}
            var response = await _roleService.GetAllRole(ownerParameters);
            var metadata = new
            {
                response.Data.TotalCount,
                response.Data.PageSize,
                response.Data.CurrentPage,
                response.Data.TotalPages,
                response.Data.HasNext,
                response.Data.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            return Ok(response);
        }
        //[Authorize]
        [HttpPost("CreateRole")]
        public async Task<ActionResult<ServiceResponse<RoleDto>>> createRole(CreateRoleDto createRoleDto)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _roleService.createRole(createRoleDto);
            return Ok(response);
        }
        //[Authorize]
        [HttpPut("UpdateRole")]
        public async Task<ActionResult<ServiceResponse<RoleDto>>> updateRole(CreateRoleDto createRoleDto, int roleId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _roleService.updateRole(createRoleDto, roleId);
            return Ok(response);
        }
        //[Authorize]
        [HttpDelete("DeleteRole")]
        public async Task<ActionResult<ServiceResponse<RoleDto>>> deleteRole(int roleId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _roleService.deleteRole(roleId);
            return Ok(response);
        }
    }
}
