﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Service.AuthService;
using ManageEmployee.Service.EmployeeService;
using ManageEmployeeDataAccessLayer.Data;
using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace ManageEmployee.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    [Produces("application/json")]
    public class EmployeeController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IEmployeeService _employeeService;
        private readonly IConfiguration _configuration;

        public EmployeeController(DataContext dataContext, IEmployeeService employeeService, IConfiguration configuration)
        {
            _dataContext = dataContext;
            _employeeService = employeeService;
            _configuration = configuration;
        }
        [HttpGet("GetEmployeeIdthier")]
        public async Task<ActionResult<ServiceResponse<EmployeeIdTheir>>> getEmployeeIdthier()
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Employee" && userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _employeeService.getEmployeeIdthier(userId);
            return Ok(response);
        }
        [HttpPut("UpdateEmployeeId")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> updateEmployeeId(int employeeId, UEmployeeDto uEmployeeDto)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Employee" && userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _employeeService.updateEmployeeId(employeeId, uEmployeeDto);
            return Ok(response);
        }
        [HttpGet("GetEmployeeGroupNameAsync")]
        public async Task<ActionResult<ServiceResponse<string>>> GetEmployeeGroupNameAsync(int employeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Employee" && userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _employeeService.GetEmployeeGroupNameAsync(employeeId);
            return Ok(response);
        }
        
        [HttpGet("GetEmployeeIdOther")] 
        public async Task<ActionResult<ServiceResponse<UEmployeeDto>>> getEmployeeIdOther(int employeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Employee" && userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _employeeService.getEmployeeIdOther(employeeId);
            return Ok(response);
        }

        
        [HttpPut("Updatepass")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> updatepass(string newPass)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Employee" && userRole != "Admin" && userRole != "Leader")
            {
                return new StatusCodeResult(403);
            }
            var response = await _employeeService.updatepass(userId, newPass);
            return Ok(response);
        }
    }
}
