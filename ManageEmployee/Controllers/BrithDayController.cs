﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Service.AuthService;
using ManageEmployee.Service.BirthDayService;
using ManageEmployeeDataAccessLayer.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageEmployee.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrithDayController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IBirthDayService _birthDayService;

        public BrithDayController(DataContext dataContext, IBirthDayService birthDayService)
        {
            _dataContext = dataContext;
            _birthDayService = birthDayService;
        }
        [HttpGet("GetBrithDay")]
        public async Task<ActionResult<ServiceResponse<BirthDayDto>>> getBrithDay()
        {
            var response = await _birthDayService.getBrithDay();
            return Ok(response);
        }
        [HttpGet("GetBrithDayMonth")]
        public async Task<ActionResult<ServiceResponse<BirthDayMonthDto>>> getBrithDayMonth()
        {
            var response = await _birthDayService.getBrithDayMonth();
            return Ok(response);
        }
    }
}
