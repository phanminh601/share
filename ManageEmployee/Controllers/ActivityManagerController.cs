﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployee.Service.ActivityService;
using ManageEmployee.Service.GroupService;
using ManageEmployeeDataAccessLayer.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Security.Claims;

namespace ManageEmployee.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    [Produces("application/json")]
    public class ActivityManagerController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IActivityService _activityService;

        public ActivityManagerController(DataContext dataContext, IActivityService activityService)
        {
            _dataContext = dataContext;
            _activityService = activityService;
        }
        [HttpGet("GetAllActivity")]
        public async Task<ActionResult<ServiceResponse<PageList<ActivityDto>>>> GetAllActivity([FromQuery] OwnerParameters ownerParameters)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);    
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _activityService.GetAllActivity(ownerParameters);
            var metadata = new
            {
                response.Data.TotalCount,
                response.Data.PageSize,
                response.Data.CurrentPage,
                response.Data.TotalPages,
                response.Data.HasNext,
                response.Data.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            return Ok(response);
        }

        //public async Task<ServiceResponse<PageList<ActivityDto>>> GetAllActivity(OwnerParameters ownerParameters)
        //[Authorize]
        [HttpPost("CreateActivity")]
        public async Task<ActionResult<ServiceResponse<ActivityDto>>> createActivity(CreateActivityDto createActivityDto)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _activityService.createActivity(createActivityDto);
            return Ok(response);
        }
        //[Authorize]
        [HttpPut("UpdateActivity")]
        public async Task<ActionResult<ServiceResponse<ActivityDto>>> updateActivity(CreateActivityDto createActivityDto, int activityId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _activityService.updateActivity(createActivityDto, activityId);
            return Ok(response);
        }
        //[Authorize]
        [HttpDelete("DeleteActivity")]
        public async Task<ActionResult<ServiceResponse<ActivityDto>>> deleteActivity(int activityId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _activityService.deleteActivity(activityId);
            return Ok(response);
        }
    }
}
