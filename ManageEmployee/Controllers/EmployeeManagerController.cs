﻿using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Request;
using ManageEmployee.DTOs.Response;
using ManageEmployee.Models;
using ManageEmployee.Pagination;
using ManageEmployee.Service.ManageEmployee;
using ManageEmployeeDataAccessLayer.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Security.Claims;

namespace ManageEmployee.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    [Produces("application/json")]
    public class EmployeeManagerController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IManageEmployee _manageEmployee;

        public EmployeeManagerController(DataContext dataContext, IManageEmployee manageEmployee)
        {
            _dataContext = dataContext;
            _manageEmployee = manageEmployee;
        }
        [HttpGet("GetAllEmployee")]
        //public async  Task<ActionResult<List<ServiceResponse<EmployeeLRDto>>>> GetAllEmployee(OwnerParameters ownerParameters)
        public async Task<ActionResult<ServiceResponse<PageList<EmployeeIdTheir>>>> GetAllEmployee([FromQuery] OwnerParameters ownerParameters)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.GetAllEmployee(ownerParameters);
            //var courseResponsePagedList = SettingsPagination(response);
            var metadata = new
            {
                response.Data.TotalCount,
                response.Data.PageSize,
                response.Data.CurrentPage,
                response.Data.TotalPages,
                response.Data.HasNext,
                response.Data.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            return Ok(response);
        }
        [HttpGet("GetAllEmployeeId")]
        public async Task<ActionResult<ServiceResponse<EmployeeIdTheir>>> GetAllEmployeeId(int MEmployeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.GetAllEmployeeId(MEmployeeId);
            return Ok(response);
        }
        
        //[Authorize]
        [HttpPost("CreateEmployee")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> createMEmployee( CreateEmployeeDto createEmployeeDto)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.createMEmployee(createEmployeeDto);
            return Ok(response);
        }
        //[Authorize]
        [HttpPut("UpdateEmployee")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> updateMEmployee(UpdateEmployeeDto updateEmployeeDto, int MEmployeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.updateMEmployee(updateEmployeeDto, MEmployeeId);
            return Ok(response);
        }
        //[Authorize]
        [HttpDelete("DeleteEmployee")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> deleteMEmployee(int MEmployeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.deleteMEmployee(MEmployeeId);
            return Ok(response);
        }
        //[Authorize]
        [HttpPost("AddEmployeeGroup")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> AddEmployeeGroup([FromQuery] AddEmployee addEmployee, int MEmployeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.addEmployeeGroup(addEmployee, MEmployeeId);
            return Ok(response);
        }
        //[Authorize]
        [HttpPost("AddEmployeeRole")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> addEmployeeRole([FromQuery] AddEmployee addEmployee, int MEmployeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.addEmployeeRole(addEmployee, MEmployeeId);
            return Ok(response);
        }
       
        [HttpPost("AddEmployeeActivity")]
        public async Task<ActionResult<ServiceResponse<EmployeeDto>>> addEmployeeActivity([FromQuery] AddEmployeeActivity addEmployeeActivity, int MEmployeeId)
        {
            var userId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            string userRole = CheckPermission.Check(userId);
            if (userRole != "Admin")
            {
                return new StatusCodeResult(403);
            }
            var response = await _manageEmployee.addEmployeeActivity(addEmployeeActivity, MEmployeeId);
            return Ok(response);
        }
        ////[Authorize]
        //[HttpGet("ViewEmployee}")]
        //public async Task<ActionResult<ServiceResponse<string>>> viewEmployee(int MEmployeeId)
        //{
        //    var response = await _manageEmployee.viewEmployee(MEmployeeId);
        //    return Ok(response);
        //}

    }
}
