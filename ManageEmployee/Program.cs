﻿using AutoMapper;
using ManageEmployee.HTTP.SSO.JWT;
using ManageEmployee.Service.ActivityService;
using ManageEmployee.Service.AuthService;
using ManageEmployee.Service.BirthDayService;
using ManageEmployee.Service.EmployeeService;
using ManageEmployee.Service.GroupService;
using ManageEmployee.Service.ManageEmployee;
using ManageEmployee.Service.RoleService;
using ManageEmployeeDataAccessLayer.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System.Configuration;
using System.Text;
using System.Text.Json.Serialization;
using Tensorflow;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IManageEmployee, ManageEmployeelmpt>();
builder.Services.AddScoped<IAuthService, AuthServicelmpl>();
builder.Services.AddScoped<IJwtTokenProvider, JwtTokenProvider>();
builder.Services.AddScoped<IGroupService, GroupServicelmpl>();
builder.Services.AddScoped<IActivityService, ActivityServicelmpl>();
builder.Services.AddScoped<IRoleService, RoleServicelmpl>();
builder.Services.AddScoped<IBirthDayService, BirthDayServicelmpl>();
builder.Services.AddScoped<IEmployeeService, EmployeeServicelmpl>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
{
    option.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        // IssuerSigningKey = new SymmetricSecurityKey(
        // System.Text.Encoding.UTF8.GetBytes(Configuration.GetSection("AppSettings:TokenKeySecret").Value)),
        IssuerSigningKey = new SymmetricSecurityKey(
            System.Text.Encoding.UTF8.GetBytes(builder.Configuration.GetSection("AppSettings:TokenKeySecret").Value)),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "CSM API", Version = "v1" });
    c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Description = "Standard Authorization header using the Bearer scheme, e.g. \"Bearer {token} \"",
        In = ParameterLocation.Header,
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });
    c.OperationFilter<SecurityRequirementsOperationFilter>();
});
// Kết nối MySQL
builder.Services.AddDbContext<DataContext>(options =>
{
    options.UseMySQL(builder.Configuration.GetConnectionString("DefaultConnection"));
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
// bổ su corr vì lỗi :
app.UseCors(options =>
{
    options.WithOrigins("http://localhost:3000");
    options.AllowAnyMethod();
    options.AllowAnyHeader();
});

//builder.Services.AddCors(options =>
//{
//    options.AddPolicy("CorsPolicy",
//        builder => builder.AllowAnyOrigin()
//       .AllowAnyMethod()
//       .AllowAnyHeader());
//});
//app.UseCors("CorsPolicy");
//
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
