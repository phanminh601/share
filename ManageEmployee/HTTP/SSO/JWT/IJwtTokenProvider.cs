﻿using System.Security.Principal;

namespace ManageEmployee.HTTP.SSO.JWT
{
    public interface IJwtTokenProvider
    {
        string CreateToken(IPrincipal userLogin);
    }
}
