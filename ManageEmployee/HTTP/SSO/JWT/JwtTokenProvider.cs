﻿using ManageEmployeeDataAccessLayer.Data;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;

namespace ManageEmployee.HTTP.SSO.JWT
{
    public class JwtTokenProvider : IJwtTokenProvider
    {
        private readonly DataContext _dataContext;
        private readonly IConfiguration _configuration;

        public JwtTokenProvider(DataContext dataContext, IConfiguration configuration)
        {
            _dataContext = dataContext;
            _configuration = configuration;
        }
        public string CreateToken(IPrincipal userLogin)
        {
            //throw new NotImplementedException();

            List<Claim> claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, userLogin.Identity.Name),
        };
            SymmetricSecurityKey key = new SymmetricSecurityKey(
            System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:TokenKeySecret").Value));
            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddMonths(1),
                SigningCredentials = creds,
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
