﻿using ManageEmployeeDataAccessLayer.DataObject;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManageEmployee.DTOs.Dto
{
    public class EmployeeDto
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime Date_of_birth { get; set; }

        public string Address { get; set; }

        public string Phonenumber { get; set; }

        public string Email { get; set; }

        public decimal Salary { get; set; }

        public int Manager_id { get; set; }

        // public string? Username { get; set; }

        // public string Password { get; set; }

        public string Avatar { get; set; }

    }
    public class CreateEmployeeDto
    {
        //public int Id { get; set; }

        public string Name { get; set; } = null!;

        public string? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string? Address { get; set; }

        public string? Phonenumber { get; set; }

        public string? Email { get; set; }

        public decimal? Salary { get; set; }

        public int? ManagerId { get; set; }

        public string Username { get; set; } = null!;

        public string? Password { get; set; }

        public string? Avatar { get; set; }

    }
    public class EmployeeLRDto
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public List<EmployeeDto> employee { get; set; }
    }
    public class EmployeeDTOUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class UpdateEmployeeDto
    {
        //public int ID { get; set; }
        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime Date_of_birth { get; set; }

        public string Address { get; set; }

        public string Phonenumber { get; set; }

        public string Email { get; set; }

        public decimal Salary { get; set; }

        public int? Manager_id { get; set; }

        //public string? Username { get; set; }

        //public string Password { get; set; }

        public string? Avatar { get; set; }
    }
    public class AddEmployee // add role, group
    {
        public int Id { get; set; }
    }
    public class AddEmployeeActivity // add role, group
    {
        public int IdGroup { get; set; }
        public int IdACtivity { get; set; }
    }

    public class ViewEmployeeDto
    {
        public string EmployeeName { get; set; }
        public List<string> NameGroup { get; set; }
        public List<string> NameRole { get; set; }
        public List<string> NameActivity { get; set; }
    }
    public class UEmployeeDto // xem tt của nv khác 
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime Date_of_birth { get; set; }

        public string Address { get; set; }

        public string Phonenumber { get; set; }

        public string Email { get; set; }

    }

    public class EmployeeIdTheir // xem tt của bản thân 
    {
        public int Id { get; set; }

        public string Name { get; set; } = null!;

        public string? Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string? Address { get; set; }

        public string? Phonenumber { get; set; }

        public string? Email { get; set; }

        public decimal? Salary { get; set; }

        public int? ManagerId { get; set; }

        public string Username { get; set; } = null!;

        public string? Password { get; set; }

        public string? Avatar { get; set; }

        //public virtual ICollection<EmployeeGroup> EmployeeGroups { get; set; }

        //public virtual ICollection<EmployeeRole> EmployeeRoles { get; set; }

        //public virtual ICollection<Employee> InverseManager { get; set; }

        //public virtual Employee? Manager { get; set; }
    }
}
