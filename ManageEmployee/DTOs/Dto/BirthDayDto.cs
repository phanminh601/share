﻿namespace ManageEmployee.DTOs.Dto
{
    public class BirthDayDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateOfBirh { get; set; }
        //public int Age { get; set; }
        //public int CountBirth {  get; set; }
    }
    public class BirthDayMonthDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateOfBirh { get; set; }
        public int CountBirth { get; set; }
    }
}
