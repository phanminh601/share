﻿namespace ManageEmployee.DTOs.Dto
{
    public class GroupDto
    {
        public int IdGroup { get; set; }

        public string NameGroup { get; set; } = null!;

        public string Leader { get; set; } = null!;

        public string? Description { get; set; }
    }
    public class CreateGroupDto{
       // public int IdGroup { get; set; }

        public string NameGroup { get; set; } = null!;

        public string Leader { get; set; } = null!;

       public string? Description { get; set; }
     }
    public class UpdateGroupDto{
       // public int IdGroup { get; set; }

        public string NameGroup { get; set; } = null!;

        public string Leader { get; set; } = null!;

        public string? Description { get; set; }
    }
        
}
