﻿namespace ManageEmployee.DTOs.Dto
{
    public class RoleDto
    {
        public int IdRole { get; set; }

        public string NameRole { get; set; } = null!;

        public string? Description { get; set; }
    }
    public class CreateRoleDto
    {
        //public int IdRole { get; set; }

        public string NameRole { get; set; } = null!;
        public string? Description { get; set; }
    }
}
