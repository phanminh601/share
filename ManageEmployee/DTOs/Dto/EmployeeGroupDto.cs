﻿namespace ManageEmployee.DTOs.Dto
{
    public class EmployeeGroupDto
    {
        public int? EmployeeId { get; set; }

        public int? IdGroup { get; set; }

        public int? IdActivity { get; set; }

        public int EmployeeGroupId { get; set; }
    }
}
