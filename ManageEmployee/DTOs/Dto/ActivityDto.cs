﻿namespace ManageEmployee.DTOs.Dto
{
    public class ActivityDto
    {
        public int IdActivity { get; set; }

        public string NameActivity { get; set; } = null!;

        public string? Description { get; set; }

        public DateTime? ParticipationDate { get; set; }

        public string? Status { get; set; }
    }
    public class CreateActivityDto
    {
       // public int IdActivity { get; set; }

        public string NameActivity { get; set; } = null!;

        public string? Description { get; set; }

        public DateTime? ParticipationDate { get; set; }

        public string? Status { get; set; }
    }

}
