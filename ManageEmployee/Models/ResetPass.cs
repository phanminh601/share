﻿using System.ComponentModel.DataAnnotations;

namespace ManageEmployee.Models
{
    public class ResetPass
    {
        [Required] public string currentPassword { get; set; }
        [Required] public string newPassword { get; set; }
    }
}
