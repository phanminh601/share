﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Google.Apis.Drive.v3.Data;
using ManageEmployeeDataAccessLayer.Data;

namespace ManageEmployee.Models
{
    public class CheckPermission
    {
        public static string Check(int userId)
        {
            DataContext _dataContext = new DataContext();
            //var userIdLogin = _dataContext.Employees
            //    .Where(e => e.Username == userId)
            //    .Select(e => e.Id).FirstOrDefault();

            var userRole = (from e in _dataContext.Employees
                            join er in _dataContext.EmployeeRoles on e.Id equals er.RoleId
                            join r in _dataContext.Roles on er.IdRole equals r.IdRole
                            where e.Id == userId
                            select r.NameRole).FirstOrDefault();
            return userRole;
        }
    }
}


