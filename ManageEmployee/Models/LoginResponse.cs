﻿using ManageEmployee.DTOs.Dto;

namespace ManageEmployee.Models
{
    //public class LoginResponse
    //{
    //    public string accessToken { get; set; }

    //    public string tokenType { get; set; } = "Bearer";
    //    public string RoleName { get; set; }
    //    //public int? StatusCode { get; set; } = 200;
    //    //public bool Status { get; set; } = true;
    //    //public string Message { get; set; } = string.Empty;
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public DateTime? DateOfBirh { get; set; }
    //}
    public class LoginResponse<T>
    {
        public LoginResponse()
        {
        }
        public LoginResponse(Boolean status)
        {
            if (!status)
            {
                Status = false;
                ErrorCode = 400;
            }
        }
        public T? Data { get; set; }
        public string accessToken { get; set; }
        public string tokenType { get; set; } = "Bearer";
        public string RoleName { get; set; }
        public int Id { get; set; }

        public int ErrorCode { get; set; } = 200;
        public bool Status { get; set; } = true;
        public string Message { get; set; } = "OK";
        public string Messageed { get; set; } = "OK";
    }

}
