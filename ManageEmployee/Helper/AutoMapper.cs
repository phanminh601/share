﻿using AutoMapper;
using ManageEmployee.DTOs.Dto;
using ManageEmployee.DTOs.Request;
using ManageEmployee.Models;
using ManageEmployeeDataAccessLayer.DataObject;
using System.Diagnostics;

namespace ManageEmployee.Helper
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<Employee, EmployeeDto>().ReverseMap();
            CreateMap<Employee, LoginFrom>().ReverseMap();
            CreateMap<LoginFrom, EmployeeDto>().ReverseMap();
            CreateMap<EmployeeIdTheir, Employee>().ReverseMap();
            CreateMap<UEmployeeDto, Employee>().ReverseMap();
            CreateMap<Employee, ResetPass>().ReverseMap();
            CreateMap<Groupp, GroupDto>().ReverseMap();
            CreateMap<Activityy, ActivityDto>().ReverseMap();
            CreateMap<Role, RoleDto>().ReverseMap();
            CreateMap<Role, CreateRoleDto>().ReverseMap();
            CreateMap<CreateActivityDto, Activityy>().ReverseMap();
            CreateMap<CreateEmployeeDto, Employee>().ReverseMap();
            CreateMap<UpdateEmployeeDto, Employee>().ReverseMap();
            CreateMap<CreateGroupDto, Groupp>().ReverseMap();
            CreateMap<UpdateGroupDto, Groupp>().ReverseMap();
            CreateMap<EmployeeLRDto, Employee>().ReverseMap();
            CreateMap<AddEmployee, EmployeeGroup>().ReverseMap();
        }
    }
}
